#include <Arduino.h>

const int POTI_PIN = A0;
const int POTI_RESOLUTION = 1023;

const byte OC1A_PIN = 9;
const byte OC1B_PIN = 10;
const word PWM_FREQ_HZ = 25000; 
const word TCNT1_TOP = 16000000/(2*PWM_FREQ_HZ);  // Upper Limit for PWM (=320)

void setup()
{
  // Pin-Modes
  pinMode(OC1A_PIN, OUTPUT);
  pinMode(OC1B_PIN, OUTPUT);

  // PWM settings
  // Clear Timer1 control and count registers
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1  = 0;

  TCCR1A |= (1 << COM1A1) | (1 << WGM11);
  TCCR1B |= (1 << WGM13) | (1 << CS10);
  ICR1 = TCNT1_TOP;

  // Serial Communication
  Serial.begin(9600);
}

void setPwmDuty(int poti_value)
{
  unsigned duty_val = map(poti_value, 0, 1023, 100, TCNT1_TOP);
  OCR1A = (word) duty_val;
}


void loop()
{
  int poti_value;

  // Read potentiometer-value
  poti_value = analogRead(POTI_PIN);

  // Set PWM
  setPwmDuty(poti_value);
}